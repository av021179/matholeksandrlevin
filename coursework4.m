subplot(2,2,1);
x = (0:0.01:8)*2*pi;
y = 9*exp(-x/8);
plot (x,y);
subplot(2,2,2);
 x= 1:10; 
loglog(x, x.^4);
subplot(2,2,3);
x = (-9:0.01:9)*2*pi;
y=sinh(3*x/9);
plot(x,y);
subplot(2,2,4);
x = (-8:0.01:8)*2*pi;
y=cosh(4*x/8);
plot(x,y);


